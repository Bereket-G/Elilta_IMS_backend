// ~ Load Module Dependecies ~ //

var events = require('events');
var SalesDal = require('../dal/sales_d');


exports.getAllSales = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var offset = parseInt(req.query.offset);
    var limit  = parseInt(req.query.limit);
    workflow.on('performRequest', function performRequest() {
        SalesDal.getAll(offset,limit, function callback(err, Sales) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sales);
        });
    });

    workflow.emit('performRequest');

};


exports.searchSales = function (req, res, next) {
    var workflow = new events.EventEmitter();

    var keyword = req.query.keyword;

    workflow.on('performRequest', function performRequest() {
        SalesDal.getWithKeyWord(keyword , function callback(err, Sales) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sales);
        });
    });

    workflow.emit('performRequest');

};

exports.addSales = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('bill_to', 'bill_to is empty').notEmpty();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        SalesDal.registerSale(req.body, function callback(err, Sales) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sales);
        });

    });

    workflow.emit('validateRequest');

};

exports.deleteSales = function (req, res, next) {

    var workflow = new events.EventEmitter();
    var _query_ = { id : req.params.id};
    workflow.on('performRequest', function performRequest() {
        SalesDal.deleteSale(_query_ , function callback(err, no_rows_affected) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.send({'Sales_deleted': no_rows_affected});
        });
    });

    workflow.emit('performRequest');
};

exports.updateSales = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var SalesId = req.body.id;
    var body  = req.body;

console.log(req.body);
    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('bill_to', 'bill_to is empty').notEmpty();
        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {
        SalesDal.updateSale(SalesId, body, function callback(err, rows_updated) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json({'Sales updated ': rows_updated});
        });
    });

    workflow.emit('validateRequest');
};

exports.getSales = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        SalesDal.findSale(req.body, function callback(err, Sales) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sales);
        });

    });

    workflow.emit('performRequest');

};


exports.getTodaySale = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        SalesDal.getTodaysSale(req.body, function callback(err, Sales) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sales);
        });

    });

    workflow.emit('performRequest');

};

exports.getMonthlyReportSales = function (req, res, next) {

    var workflow = new events.EventEmitter();
    var offset = parseInt(req.query.offset);
    var limit  = parseInt(req.query.limit);
    workflow.on('performRequest', function performRequest() {

        SalesDal.findMonthlyReportSales(limit, offset, function callback(err, Sales) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sales);
        });

    });

    workflow.emit('performRequest');

};


exports.getWeeklyReportSales = function (req, res, next) {

    var workflow = new events.EventEmitter();
    var offset = parseInt(req.query.offset);
    var limit  = parseInt(req.query.limit);
    workflow.on('performRequest', function performRequest() {

        SalesDal.findWeeklyReportSales(limit, offset, function callback(err, Sales) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sales);
        });

    });

    workflow.emit('performRequest');

};


exports.getDailyReportSales = function (req, res, next) {

    var workflow = new events.EventEmitter();
    var offset = parseInt(req.query.offset);
    var limit  = parseInt(req.query.limit);
    workflow.on('performRequest', function performRequest() {

        SalesDal.findDailyReportSales(limit, offset, function callback(err, Sales) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sales);
        });

    });

    workflow.emit('performRequest');

};

exports.findMonthlySoldItemsReport = function (req, res, next) {

    var workflow = new events.EventEmitter();
    var month_number = parseInt(req.query.month_number);
    var year  = parseInt(req.query.year);
    workflow.on('performRequest', function performRequest() {

        SalesDal.findMonthlySoldItemsReport(month_number, year, function callback(err, result) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(result);
        });

    });

    workflow.emit('performRequest');

};


exports.getSaleWithDateRange = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        SalesDal.findSaleWithDateRange(req.body, function callback(err, Sales) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sales);
        });

    });

    workflow.emit('performRequest');

};



exports.getcurrentFSNO = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        SalesDal.getcurrentFSNO([], function callback(err, Sales) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sales);
        });

    });

    workflow.emit('performRequest');

};