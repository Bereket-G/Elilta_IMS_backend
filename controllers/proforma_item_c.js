// ~ Load Module Dependecies ~ //

var events = require('events');
var Proforma_itemsDal = require('../dal/proforma_item_d');


exports.getAllProforma_items = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        Proforma_itemsDal.getAll({}, function callback(err, Proforma_items) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Proforma_items);
        });
    });

    workflow.emit('performRequest');

};

exports.addProforma_item = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('quantity', 'name is invalid').notEmpty().isNumeric();

        req.checkBody('item_price', 'item price is invalid').notEmpty().isFloat();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        Proforma_itemsDal.registerProforma_item(req.body, function callback(err, Proforma_item) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Proforma_item);
        });

    });

    workflow.emit('validateRequest');

};

exports.deleteProforma_item = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        Proforma_itemsDal.deleteProforma_item(req.body , function callback(err, no_rows_affected) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.send({'Proforma_items_deleted': no_rows_affected});
        });
    });

    workflow.emit('performRequest');
};

exports.updateProforma_item = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var Proforma_itemId = req.body.id;
    var body  = req.body;

console.log(req.body);
    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('quantity', 'name is invalid').notEmpty().isNumeric();

        req.checkBody('item_price', 'item price is invalid').notEmpty().isFloat();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {
        Proforma_itemsDal.updateProforma_item(Proforma_itemId, body, function callback(err, rows_updated) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json({'Proforma_items updated ': rows_updated});
        });
    });

    workflow.emit('validateRequest');
};

exports.getProforma_item = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        Proforma_itemsDal.findProforma_items(req.body, function callback(err, Proforma_items) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Proforma_items);
        });

    });

    workflow.emit('performRequest');

};
