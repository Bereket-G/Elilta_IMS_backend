// ~ Load Module Dependecies ~ //

var events = require('events');
var CategoriesDal = require('../dal/categories_d');


exports.getAllCategories = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        CategoriesDal.getAll({}, function callback(err, Categories) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Categories);
        });
    });

    workflow.emit('performRequest');

};

exports.addCategory = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'name is empty').notEmpty();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        CategoriesDal.registerCategory(req.body, function callback(err, Category) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Category);
        });

    });

    workflow.emit('validateRequest');

};

exports.deleteCategory = function (req, res, next) {

    var workflow = new events.EventEmitter();
    var _query_ = { id : req.params.id};
    workflow.on('performRequest', function performRequest() {
        CategoriesDal.deleteCategory(_query_ , function callback(err, no_rows_affected) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.send({'Categories_deleted': no_rows_affected});
        });
    });

    workflow.emit('performRequest');
};

exports.updateCategory = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var CategoryId = req.body.id;
    var body  = req.body;

console.log(req.body);
    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'name is empty').notEmpty();
        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {
        CategoriesDal.updateCategory(CategoryId, body, function callback(err, rows_updated) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json({'Categories updated ': rows_updated});
        });
    });

    workflow.emit('validateRequest');
};

exports.getCategory = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        CategoriesDal.findCategory(req.body, function callback(err, Categories) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Categories);
        });

    });

    workflow.emit('performRequest');

};
