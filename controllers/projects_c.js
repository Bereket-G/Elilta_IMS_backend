// ~ Load Module Dependecies ~ //

var events = require('events');
var ProjectsDal = require('../dal/projects_d');


exports.getAllProjects = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        ProjectsDal.getAll({}, function callback(err, Projects) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Projects);
        });
    });

    workflow.emit('performRequest');

};

exports.addProject = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'Project Name can not be empty').notEmpty();
        req.checkBody('amount', 'Project Budget amount can not be empty').notEmpty();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        ProjectsDal.registerProject(req.body, function callback(err, Project) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Project);
        });

    });

    workflow.emit('validateRequest');

};

exports.deleteProject = function (req, res, next) {

    var workflow = new events.EventEmitter();
    var _query_ = { id : req.params.id};
    workflow.on('performRequest', function performRequest() {
        ProjectsDal.deleteProject(_query_ , function callback(err, no_rows_affected) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.send({'Projects_deleted': no_rows_affected});
        });
    });

    workflow.emit('performRequest');
};

exports.updateProject = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var ProjectId = req.body.id;
    var body  = req.body;

console.log(req.body);
    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'name is empty').notEmpty();
        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {
        ProjectsDal.updateProject(ProjectId, body, function callback(err, rows_updated) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json({'Projects updated ': rows_updated});
        });
    });

    workflow.emit('validateRequest');
};

exports.getProject = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        ProjectsDal.findProject(req.body, function callback(err, Projects) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Projects);
        });

    });

    workflow.emit('performRequest');

};


exports.getProjectAllocatedItems = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        ProjectsDal.findProjectItems(req.body, function callback(err, Projects) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Projects);
        });

    });

    workflow.emit('performRequest');

};

exports.updateProjectAllocatedItems = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        ProjectsDal.updateProjectAllocatedItems(req.body, function callback(err, Projects) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Projects);
        });

    });

    workflow.emit('performRequest');

};
