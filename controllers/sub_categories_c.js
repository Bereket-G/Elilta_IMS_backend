// ~ Load Module Dependecies ~ //

var events = require('events');
var Sub_categoriesDal = require('../dal/sub_categories_d');


exports.getAllSub_categories = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        Sub_categoriesDal.getAll({}, function callback(err, Sub_categories) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sub_categories);
        });
    });

    workflow.emit('performRequest');

};

exports.addSub_category = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'name is empty').notEmpty();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        Sub_categoriesDal.registersub_category(req.body, function callback(err, Sub_category) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sub_category);
        });

    });

    workflow.emit('validateRequest');

};

exports.deleteSub_category = function (req, res, next) {

    var workflow = new events.EventEmitter();
    var _query_ = { id : req.params.id};
    workflow.on('performRequest', function performRequest() {
        Sub_categoriesDal.deletesub_category(_query_ , function callback(err, no_rows_affected) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.send({'Sub_categories_deleted': no_rows_affected});
        });
    });

    workflow.emit('performRequest');
};

exports.updateSub_category = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var Sub_categoryId = req.body.id;
    var body  = req.body;

console.log(req.body);
    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'name is empty').notEmpty();
        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {
        Sub_categoriesDal.updatesub_category(Sub_categoryId, body, function callback(err, rows_updated) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;

            }

            res.status(201);
            res.json({'Sub_categories updated ': rows_updated});
        });
    });

    workflow.emit('validateRequest');
};

exports.getSub_category = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        Sub_categoriesDal.findsub_category(req.query, function callback(err, Sub_categories) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Sub_categories);
        });

    });

    workflow.emit('performRequest');

};
