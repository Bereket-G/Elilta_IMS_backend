// ~ Load Module Dependecies ~ //

var events = require('events');
var Stock_itemsDal = require('../dal/stock_items_d');


exports.getAllStock_items = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        Stock_itemsDal.getAll({}, function callback(err, Stock_items) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Stock_items);
        });
    });

    workflow.emit('performRequest');

};

exports.getAllStockInHistory = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        Stock_itemsDal.getAllStockInHistory({}, function callback(err, Stock_items) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Stock_items);
        });
    });

    workflow.emit('performRequest');

};


exports.addStock_item = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('quantity', 'quantity is empty').notEmpty();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        Stock_itemsDal.registerStock_item(req.body, function callback(err, Stock_item) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Stock_item);
        });

    });

    workflow.emit('validateRequest');

};

exports.deleteStock_item = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        Stock_itemsDal.deleteStock_item(req.body , function callback(err, no_rows_affected) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.send({'Stock_items_deleted': no_rows_affected});
        });
    });

    workflow.emit('performRequest');
};

exports.updateStock_item = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var Stock_itemId = req.body.id;
    var body  = req.body;

    workflow.on('validateRequest', function validateRequest() {

        req.checkBody('quantity', 'quantity is empty').notEmpty();
        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {
        Stock_itemsDal.updateStock_item(Stock_itemId, body, function callback(err, rows_updated) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;

            }

            res.status(201);
            res.json({'Stock_items updated ': rows_updated});
        });
    });

    workflow.emit('validateRequest');
};

exports.getStock_item = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        Stock_itemsDal.findStock_item(req.body, function callback(err, Stock_items) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Stock_items);
        });

    });

    workflow.emit('performRequest');

};

exports.getStockItemsByCategory = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        Stock_itemsDal.findStockItemsByCategory(req.body, function callback(err, Stock_items) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Stock_items);
        });

    });

    workflow.emit('performRequest');

};


exports.getStockItemsByProduct = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        Stock_itemsDal.findStockItemsByProduct(req.body, function callback(err, Stock_items) {
            if(err){
                res.status(500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(Stock_items);
        });

    });

    workflow.emit('performRequest');

};


exports.checkItemAvailability = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        Stock_itemsDal._checkItemAvailability(req.body, function callback(err, msg) {
            if(err){
                res.status(err.status || 500);
                res.send({'message': err.message});
                return;
            }

            try {
                res.status(201);
                res.send({'message': msg.message});
            } catch (e){
                console.log('Error while sending request.')
            }

        });

    });

    workflow.emit('performRequest');

};

exports.saveSale = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        Stock_itemsDal._saveSale(req.body, function callback(err, msg) {
            if(err){
                res.status(err.status || 500);
                res.send({'message': err.message});
                return;
            }

            res.status(201);
            res.json(msg);
        });

    });

    workflow.emit('performRequest');

};