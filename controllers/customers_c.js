// ~ Load Module Dependecies ~ //

var events = require('events');
var customersDal = require('../dal/customers_d');


exports.getAllCustomers = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {
        customersDal.getAll({}, function callback(err, customers) {
            if(err){
                res.status(err.statusCode || 500);
                res.send({'msg': err.message});
                return;
            }

            res.status(201);
            res.json(customers);
        });
    });

    workflow.emit('performRequest');

};

exports.addCustomer = function (req, res, next) {
    var workflow = new events.EventEmitter();

    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'name is empty').notEmpty();

        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {

        customersDal.registerCustomer(req.body, function callback(err, Customer) {
            if(err){
                res.status(500);
                res.send({'msg': err.message});
                return;
            }

            res.status(201);
            res.json(Customer);
        });

    });

    workflow.emit('validateRequest');

};

exports.deleteCustomer = function (req, res, next) {

    var workflow = new events.EventEmitter();
    var _query_ = { id : req.params.id};
    workflow.on('performRequest', function performRequest() {
        customersDal.deleteCustomer(_query_ , function callback(err, no_rows_affected) {
            if(err){
                res.status(500);
                res.send({'msg': err.message});
                return;
            }

            res.status(201);
            res.send({'customers_deleted': no_rows_affected});
        });
    });

    workflow.emit('performRequest');
};

exports.updateCustomer = function (req, res, next) {
    var workflow = new events.EventEmitter();
    var CustomerId = req.body.id;
    var body  = req.body;

console.log(req.body);
    workflow.on('validateRequest', function validateRequest() {
        req.checkBody('name', 'name is empty').notEmpty();

        req.checkBody('TIN_NO', 'TIN_NO is not valid').notEmpty().isNumeric();
        var validationErrors = req.validationErrors();

        if(validationErrors){
            res.status(400);
            res.json(validationErrors);
        } else {
            workflow.emit('performRequest');
        }
    });

    workflow.on('performRequest', function performRequest() {
        customersDal.updateCustomer(CustomerId, body, function callback(err, rows_updated) {
            if(err){
                res.status(500);
                res.send({'msg': err.message});
                return;

            }

            res.status(201);
            res.json({'customers updated ': rows_updated});
        });
    });

    workflow.emit('validateRequest');
};

exports.getCustomer = function (req, res, next) {

    var workflow = new events.EventEmitter();

    workflow.on('performRequest', function performRequest() {

        customersDal.findCustomers(req.body, function callback(err, customers) {
            if(err){
                res.status(500);
                res.send({'msg': err.message});
                return;
            }

            res.status(201);
            res.json(customers);
        });

    });

    workflow.emit('performRequest');

};
