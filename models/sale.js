'use strict';
module.exports = (sequelize, DataTypes) => {
  var sale = sequelize.define('sale', {
    bill_to: DataTypes.STRING,
    TIN_NO: DataTypes.STRING,
    address: DataTypes.STRING,
    sale_date: DataTypes.DATE,
    reference: DataTypes.STRING,
    FSNO: DataTypes.STRING,
    discount: DataTypes.FLOAT,
    amount: DataTypes.BIGINT,
    withVat: DataTypes.BOOLEAN,
    deleted : DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return sale;
};