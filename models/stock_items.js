'use strict';
module.exports = (sequelize, DataTypes) => {
  var stock_items = sequelize.define('stock_items', {
    itemId: DataTypes.INTEGER,
    reoder_level: DataTypes.INTEGER,
    quantity: DataTypes.INTEGER,
    value: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return stock_items;
};