'use strict';
module.exports = (sequelize, DataTypes) => {
  var projectItems = sequelize.define('projectitems', {
    project_Id: DataTypes.INTEGER,
      itemId: DataTypes.INTEGER,
    quantity: DataTypes.BIGINT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return projectItems;
};