'use strict';
module.exports = (sequelize, DataTypes) => {
  var proforma_items = sequelize.define('proforma_items', {
    proforma_ID: DataTypes.INTEGER,
    itemId: DataTypes.INTEGER,
    quantity: DataTypes.INTEGER,
    item_price: DataTypes.FLOAT,
    unit: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return proforma_items;
};