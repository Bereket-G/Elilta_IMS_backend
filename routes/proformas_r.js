// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();

var proformas_c = require('../controllers/proformas_c');

// ~ get all Items ~ //
router.get('/', proformas_c.getAllProformas);

// ~ search proforma table for a string ~ //
router.get('/proformaWithKeyword', proformas_c.searchProforma);

// ~ delete request from the search result ~ //
router.delete('/proformaWithKeyword/:id', proformas_c.deleteProforma);

// ~ get item with query ~ //
router.post('/find', proformas_c.getProforma);

// ~ query proforma with date range ~ //
router.post('/findByDateRange', proformas_c.getProformaWithDateRange);

// ~ register new item ~ //
router.post('/', proformas_c.addProforma);

// ~ register new proforma ~ //
router.post('/saveProforma', proformas_c.addNewProforma);

// ~ update an item ~ //
router.put('/update', proformas_c.updateProforma);

// ~ delete an item
router.delete('/:id', proformas_c.deleteProforma);

// ~ Exports Router ~ //
module.exports = router;