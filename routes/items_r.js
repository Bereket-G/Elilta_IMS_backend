// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();

var items_c = require('../controllers/items_c');

// ~ get all Items ~ //
router.get('/', items_c.getAllItems);

// ~ get item with query ~ //
router.post('/find', items_c.getItem);

// ~ get item with like query ~ //
router.post('/findLike', items_c.getItemLike);

// ~ register new item ~ //
router.post('/', items_c.addItem);

// ~ update an item ~ //
router.put('/:id', items_c.updateItem);

// ~ delete an item
router.delete('/:id', items_c.deleteItem);

// ~ Exports Router ~ //
module.exports = router;