// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();

var stock_c = require('../controllers/stock_c');

// ~ get all Items ~ //
router.get('/getStockDetails', stock_c.getStockDetail);

// ~ get items below their reorder level ~ //
router.get('/getItemsLowInStock', stock_c.getItemsLowInStock);

// ~ Exports Router ~ //
module.exports = router;