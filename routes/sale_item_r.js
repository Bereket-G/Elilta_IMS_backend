// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();
var sale_items_c = require('../controllers/sale_items_c');

// ~ get all Items ~ //
router.get('/all', sale_items_c.getAllSale_items);

// ~ get item with query ~ //
router.post('/', sale_items_c.getSale_item);

// ~ register new item ~ //
router.post('/add', sale_items_c.addSale_item);

// ~ update an item ~ //
router.put('/update', sale_items_c.updateSale_item);

// ~ delete an item
router.delete('/delete', sale_items_c.deleteSale_item);

// ~ Exports Router ~ //
module.exports = router;