// ~ Load Module Dependencies ~ //
var express = require('express');

// ~ create a router ~ //
var router = express.Router();

var categories_c = require('../controllers/categories_c');

// ~ get all Items ~ //
router.get('/', categories_c.getAllCategories);

// ~ get item with query ~ //
router.post('/find', categories_c.getCategory);

// ~ register new item ~ //
router.post('/', categories_c.addCategory);

// ~ update an item ~ //
router.put('/update', categories_c.updateCategory);

// ~ delete an item
router.delete('/:id', categories_c.deleteCategory);

// ~ Exports Router ~ //
module.exports = router;