// ~ Load Module Dependencies ~ //

var models = require('../models');

var Proforma_itemModel = models.proforma_items;


exports.getAll = function getAll(query, cb) {

  Proforma_itemModel.findAll().then(function (Proforma_items) {
        cb(null,Proforma_items);
    }).catch( function (err) {
        cb(err,null);
    });

};

exports.registerProforma_item = function registerProforma_item(Proforma_itemData, cb) {

    Proforma_itemModel.create(Proforma_itemData).then(function (Proforma_item) {
       cb(null,Proforma_item);
    }).catch( function (err) {
       cb(err,null);
    });

};

exports.deleteProforma_item = function deleteProforma_item(query, cb) {
    Proforma_itemModel.destroy({
        where: query
    }).then(function (no_rows_affected) {
        cb(null,no_rows_affected);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findProforma_items = function findProforma_item(query, cb) {
    Proforma_itemModel.findAll({
        where: query
    }).then(function (Proforma_item) {
        cb(null,Proforma_item);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.updateProforma_item = function updateProforma_item(id, updates, cb) {
    Proforma_itemModel.update(updates,{where: {'id':id}}).then( function (rows_updated) {
        cb(null,rows_updated);
    }).catch(function (err) {
        cb(err,null);
    });
};