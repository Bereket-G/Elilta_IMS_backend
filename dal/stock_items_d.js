// ~ Load Module Dependencies ~ //

var models = require('../models');

var Stock_itemModel = models.stock_items;
var categoryModel = models.category;
var SaleModel = models.sale;
var Sale_itemModel = models.sale_items;
var stock_ins_Model = models.stock_ins;
var sequelize = models.sequelize;


exports.getAll = function getAll(query, cb) {

  Stock_itemModel.findAll().then(function (Stock_items) {
        cb(null,Stock_items);
    }).catch( function (err) {
        cb(err,null);
    });

};

exports.getAllStockInHistory = function getAll(query, cb) {

    models.sequelize.query(
        "SELECT items.id, CONCAT_WS ('' , items.name ,\" \" , items.subcategory) AS Name, stock_ins.date_in , stock_ins.quantity FROM\n" +
        "stock_ins INNER JOIN items ON (items.id = stock_ins.itemId) ORDER BY date_in DESC ",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
        cb(null, res);
    }).catch (function (err) {
        cb(err,null);
    });

    // stock_ins_Model.findAll().then(function (Stock_items) {
    //     cb(null,Stock_items);
    // }).catch( function (err) {
    //     cb(err,null);
    // });

};

exports.registerStock_item = function registerStock_item(Stock_itemData, cb) {

    var _query_ = {
        itemId : Stock_itemData.itemId
    };

    return sequelize.transaction(function (t) {

        return stock_ins_Model.create(Stock_itemData, {transaction : t}).then(function () {

            return Stock_itemModel.findAll({ where : _query_, transaction: t}).then(function (Stock_Item) {

                if(!Stock_Item[0]) {
                    return Stock_itemModel.create(Stock_itemData, {transaction: t}).then(function (Stock_item) {
                        cb(null,Stock_item);
                    }).catch( function (err) {
                        cb(err,null);
                    });
                }

                return Stock_Item[0].increment({ quantity : Stock_itemData.quantity, value : Stock_itemData.value})
                    .then(function (res) {
                        cb(null,res);
                    }).catch( function (err) {
                        cb(err,null);
                    });
            });
        });
    });



};

exports.deleteStock_item = function deleteStock_item(query, cb) {
    Stock_itemModel.destroy({
        where: query
    }).then(function (no_rows_affected) {
        cb(null,no_rows_affected);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findStock_item = function findStock_item(query, cb) {
    Stock_itemModel.findAll({
        where: query
    }).then(function (Stock_item) {
        cb(null,Stock_item);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.findStockItemsByCategory = function findStockItemsByCategory(query, cb) {

    models.sequelize.query(
        "SELECT categories.name, SUM(stock_items.quantity) AS quantities \n" +
        "FROM stock_items\n" +
        "INNER JOIN items ON (stock_items.itemId = items.id)\n" +
        "INNER JOIN categories ON (items.category = categories.name)\n" +
        "WHERE items.deleted = false\n" +
        "GROUP BY categories.name",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
            cb(null, res);
    }).catch (function (err) {
        cb(err,null);
    });

};


exports.findStockItemsByProduct = function findStockItemsByProduct(query, cb) {

    models.sequelize.query(
        "SELECT CONCAT_WS ( '' , items.name, \" \", items.subcategory) as item_name , SUM(stock_items.quantity) AS quantities\n" +
        "FROM stock_items\n" +
        "INNER JOIN items ON (stock_items.itemId = items.id)\n" +
        "WHERE items.deleted = false\n" +
        "GROUP BY item_name",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
        cb(null, res);
}).catch (function (err) {
        cb(err,null);
    });
};

exports._checkItemAvailability = function _checkItemAvailability(query, cb) {

    _query_ = {
        itemId: query.itemId
    };

    Stock_itemModel.findAll({
        where: _query_
    }, {attributes: ['quantity']}).then(function (quantity) {

        console.log(quantity);

        if(!quantity[0]){
            var err = {
                message : "Item not in Stock !",
                status : 201
            };
            cb(err,null);
            return;
        }

        if(quantity[0].quantity < query.quantity){
             err = {
                status : 201,
                message : "Not enough item in stock ! \n Only " + quantity[0].quantity + " items available in Stock"
            };
            cb(err,null);
        }

       var msg = {
            message : "Continue"
       };
        cb(null,msg);

    }).catch( function (err) {
        console.log(err);
        var err_ = {
            message : "Error !",
            status : 201
        };
        cb(err_,null);
    });
};

exports._saveSale = function _saveSale(SaleData, cb) {
    var createBillItems = [];

    console.log(SaleData);
    return sequelize.transaction(function (t) {

        // chain all your queries here. make sure you return them.
        return SaleModel.create(SaleData.saleDetail,
            {transaction: t}).then(function (sale) {

            for(var i = 0; i < SaleData.saleBillItems.length; i++){
                createBillItems.push({
                    sales_ID: sale.id,
                    itemId: SaleData.saleBillItems[i].itemId,
                    quantity: SaleData.saleBillItems[i].quantity,
                    item_price: SaleData.saleBillItems[i].item_price,
                    item_originalPrice : SaleData.saleBillItems[i].item_originalPrice,
                    unit: SaleData.saleBillItems[i].unit
                });
            }
            return Sale_itemModel.bulkCreate(
                createBillItems, {transaction: t} ).then (function (res) {
                return new Promise ( function() {

                        createBillItems.forEach( function(item, index) {
                        return  Stock_itemModel.findAll({
                                where: {itemId: item.itemId},
                                transaction: t
                            }).then(function (Stock_Item) {

                                var _value = item.quantity * item.item_originalPrice;
                                if (Stock_Item[0]) {

                                    if (Stock_Item[0].quantity <= 0) {
                                        // must not be less than zero
                                        t.rollback();
                                        cb({message: "Error quantity can not be less than 0", status: 400}, null);
                                        return;
                                    }

                                     return Stock_Item[0].decrement({quantity: item.quantity, value: _value}, {transaction: t});

                                }
                                }).then(function (res_) {
                                    try {
                                        console.log('-----');
                                        t.commit().then( function (res__) {
                                            // transaction commited successfully
                                            console.log("Transaction commited");
                                            cb(null,res);
                                        });
                                    } catch (err) {

                                    }
                                }).catch( function (err) {
                                    t.rollback();
                                    console.log(err);
                                    console.log('Error occured');
                                });

                        });
                    })
                .then( function (res_) {
                    console.log('promise returned');
                    // console.log(res_);
                    t.commit();
                });
            });

        });

    }).then(function (result) {
        // Transaction has been committed
        cb(null,result);

        console.log('transaction commited');
    }).catch(function (err) {
        // Transaction has been rolled back
        cb(err,null);
        console.log('Transaction rolled back');
    });

};


exports.updateStock_item = function updateStock_item(id, updates, cb) {
    Stock_itemModel.update(updates,{where: {'id':id}}).then( function (rows_updated) {
        cb(null,rows_updated);
    }).catch(function (err) {
        cb(err,null);
    });
};