// ~ Load Module Dependencies ~ //

var models = require('../models');

var Item_PriceModel = models.item_price;


exports.getAll = function getAll(query, cb) {

  Item_PriceModel.findAll().then(function (Item_Prices) {
        cb(null,Item_Prices);
    }).catch( function (err) {
        cb(err,null);
    });

};

exports.registerItem_Price = function registerItem_Price(Item_PriceData, cb) {

    Item_PriceModel.create(Item_PriceData).then(function (Item_Price) {
       cb(null,Item_Price);
    }).catch( function (err) {
       cb(err,null);
    });

};

exports.deleteItem_Price = function deleteItem_Price(query, cb) {
    Item_PriceModel.destroy({
        where: query
    }).then(function (no_rows_affected) {
        cb(null,no_rows_affected);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findItem_Prices = function findItem_Price(query, cb) {
    Item_PriceModel.findAll({
        where: query
    }).then(function (Item_Price) {
        cb(null,Item_Price);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.updateItem_Price = function updateItem_Price(id, updates, cb) {
    Item_PriceModel.update(updates,{where: {'id':id}}).then( function (rows_updated) {
        cb(null,rows_updated);
    }).catch(function (err) {
        cb(err,null);
    });
};