// ~ Load Module Dependencies ~ //

var models = require('../models');
var sequelize = models.sequelize;

var Stock_itemModel = models.stock_items;
var projectModel = models.project;
var Project_itemModel = models.projectitems;
var itemModel = models.items;

exports.getAll = function getAll(query, cb) {

  projectModel.findAll().then(function (projects) {
        cb(null,projects);
    }).catch( function (err) {
        cb(err,null);
    });

};

exports.registerProject = function registerProject(ProjectData, cb) {

    var createBillItems = [];

    return sequelize.transaction(function (t) {

        // chain all your queries here. make sure you return them.
        return projectModel.create(ProjectData,
            {transaction: t}).then(function (sale) {

            for(var i = 0; i < ProjectData.projectItems.length; i++){
                createBillItems.push({
                    project_Id: sale.id,
                    itemId: ProjectData.projectItems[i].itemId,
                    quantity: ProjectData.projectItems[i].quantity,
                    item_price: ProjectData.projectItems[i].item_price,
                    stock_price : ProjectData.projectItems[i].stock_price,
                    unit: ProjectData.projectItems[i].unit
                });
            }
            return Project_itemModel.bulkCreate(
                createBillItems, {transaction: t} ).then (function (res) {
                return new Promise ( function() {


                    return createBillItems.forEach( function(item, index) {
                        Stock_itemModel.findAll({
                            where: {itemId: item.itemId},
                            transaction: t
                        }).then(function (Stock_Item) {

                            console.log(index);
                            // console.log(ProjectData.projectItems[0]);

                            // console.log(t);
                            var _value = item.quantity * item.item_price;
                            if (Stock_Item[0]) {

                                if (Stock_Item[0].quantity <= 0 && ! Stock_Item[0].quantity) {
                                    // must not be less than zero
                                    t.rollback();
                                    cb({message: "Error quantity can not be less than 0", status: 400}, null);
                                    return;
                                }

                                return Stock_Item[0].decrement({quantity: item.quantity, value: _value}, {transaction: t});

                            }
                        }).then(function (res_) {
                            try {
                                console.log('-----');
                                t.commit().then( function (res__) {
                                    // transaction commited successfully
                                    console.log("Transaction commited");
                                    cb(null,{message : 'Request Sucessful'});
                                });
                            } catch (err) {

                            }
                        }).catch( function (err) {
                            t.rollback();
                            console.log(err);
                            console.log('Error occured');
                        });

                    });
                })
                    .then( function (res_) {
                        console.log('promise returned');
                        // console.log(res_);
                        t.commit();
                    });
            });

        });

    }).then(function (result) {
        // Transaction has been committed
        t.commit();
        console.log('transaction commited');
        cb(null,result);
    }).catch(function (err) {
        // Transaction has been rolled back
        console.log('Transaction rolled back');
        cb(err,null);
    });


};

exports.deleteProject = function deleteProject(query, cb) {
    projectModel.destroy({
        where: query
    }).then(function (no_rows_affected) {
        cb(null,no_rows_affected);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findProject = function findProject(query, cb) {
    projectModel.findAll({
        where: query
    }).then(function (Project) {
        cb(null,Project);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.findProjectItems = function findProject(query, cb) {
    models.sequelize.query(
        'SELECT items.custom_id, items.id, items.name, projectitems.quantity AS quantity, items.stock_price AS item_price \n' +
        'FROM `projectitems`\n' +
        'INNER JOIN items, projects \n' +
        'WHERE (items.id = projectitems.itemId && projects.id = projectitems.project_Id && projects.id = ' + query.project_Id +' )',
        {type: models.sequelize.QueryTypes.SELECT}).then(function (Project) {
        cb(null,Project);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.updateProject = function updateProject(id, updates, cb) {
    projectModel.update(updates,{where: {'id':id}}).then( function (rows_updated) {
        cb(null,rows_updated);
    }).catch(function (err) {
        cb(err,null);
    });
};

exports.updateProjectAllocatedItems = function updateProjectItemsAllocated(AllocatedItems, cb) {

    // var oldResult =

    return sequelize.transaction(function (t) {

        return sequelize.Promise.each(AllocatedItems, function(itemToUpdate){
            Project_itemModel.update(itemToUpdate, {where: {'itemId':itemToUpdate.id}} , { transaction: t })
        }).then (function (res) {
            return new Promise ( function() {

                AllocatedItems.forEach( function(item, index) {
                    Stock_itemModel.findAll({
                        where: {itemId: item.id},
                        transaction: t
                    }).then(function (Stock_Item) {

                        var _value = (item.quantity) * item.item_price;
                        if (Stock_Item[0]) {
                                console.log(Stock_Item[0].quantity);
                            if (Stock_Item[0].quantity <= 0 && ! Stock_Item[0].quantity) {
                                // must not be less than zero
                                t.rollback();
                                cb({message: "Error quantity can not be less than 0", status: 400}, null);
                                return;
                            }

                            Stock_Item[0].decrement({quantity: (item.quantity - Stock_Item[0].quantity), value: _value}, {transaction: t});

                        }
                    }).then(function (res_) {
                        try {
                            console.log('-----');
                            t.commit().then( function (res__) {
                                // transaction commited successfully
                                console.log("Transaction commited");
                                cb(null,{message : 'Request Sucessful'});
                            });
                        } catch (err) {

                        }
                    }).catch( function (err) {
                        t.rollback();
                        console.log(err);
                        console.log('Error occured');
                    });

                });
            })
                .then( function (res_) {
                    console.log('promise returned');
                    t.commit();
                });
        });

    });

};