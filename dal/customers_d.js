// ~ Load Module Dependencies ~ //

var models = require('../models');

var customerModel = models.customer;


exports.getAll = function getAll(query, cb) {

  customerModel.findAll().then(function (customers) {
        cb(null,customers);
    }).catch( function (err) {
        cb(err,null);
    });

};

exports.registerCustomer = function registercustomer(customerData, cb) {

    customerModel.create(customerData).then(function (customer) {
       cb(null,customer);
    }).catch( function (err) {
       cb(err,null);
    });

};

exports.deleteCustomer = function deletecustomer(query, cb) {
    customerModel.destroy({
        where: query
    }).then(function (no_rows_affected) {
        cb(null,no_rows_affected);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findCustomers = function findcustomer(query, cb) {
    var _name_ = query.name;
    console.log(_name_);
    customerModel.findAll({
        where: { name: { $like: '%' + _name_ + '%' } }
    }).then(function (customer) {
        cb(null,customer);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.updateCustomer = function updatecustomer(id, updates, cb) {
    customerModel.update(updates,{where: {'id':id}}).then( function (rows_updated) {
        cb(null,rows_updated);
    }).catch(function (err) {
        cb(err,null);
    });
};