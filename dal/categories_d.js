// ~ Load Module Dependencies ~ //

var models = require('../models');

var categoryModel = models.category;


exports.getAll = function getAll(query, cb) {

  categoryModel.findAll().then(function (categories) {
        cb(null,categories);
    }).catch( function (err) {
        cb(err,null);
    });

};

exports.registerCategory = function registerCategory(CategoryData, cb) {

    categoryModel.create(CategoryData).then(function (Category) {
       cb(null,Category);
    }).catch( function (err) {
       cb(err,null);
    });

};

exports.deleteCategory = function deleteCategory(query, cb) {
    categoryModel.destroy({
        where: query
    }).then(function (no_rows_affected) {
        cb(null,no_rows_affected);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findCategory = function findCategory(query, cb) {
    categoryModel.findAll({
        where: query
    }).then(function (Category) {
        cb(null,Category);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.updateCategory = function updateCategory(id, updates, cb) {
    categoryModel.update(updates,{where: {'id':id}}).then( function (rows_updated) {
        cb(null,rows_updated);
    }).catch(function (err) {
        cb(err,null);
    });
};