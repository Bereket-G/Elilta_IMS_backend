// ~ Load Module Dependencies ~ //

var models = require('../models');

var StockModel = models.stock;


exports.getStockInformation = function getStockInformation(cb) {

    models.sequelize.query(
        "SELECT * FROM stock_view",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
        cb(null, res);
    }).catch (function (err) {
        cb(err,null);
    });

};

exports.getItemsLowInStock_ = function getStockInformation(cb) {

    models.sequelize.query(
        "SELECT * FROM item_scarce_in_stock_view",
        {type: models.sequelize.QueryTypes.SELECT}).then(res => {
        cb(null, res);
}).catch (function (err) {
        cb(err,null);
    });

};