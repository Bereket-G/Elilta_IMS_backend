// ~ Load Module Dependencies ~ //

var models = require('../models');

var sub_categoryModel = models.sub_category;


exports.getAll = function getAll(query, cb) {

  sub_categoryModel.findAll().then(function (categories) {
        cb(null,categories);
    }).catch( function (err) {
        cb(err,null);
    });

};

exports.registersub_category = function registersub_category(sub_categoryData, cb) {

    sub_categoryModel.create(sub_categoryData).then(function (sub_category) {
       cb(null,sub_category);
    }).catch( function (err) {
       cb(err,null);
    });

};

exports.deletesub_category = function deletesub_category(query, cb) {
    sub_categoryModel.destroy({
        where: query
    }).then(function (no_rows_affected) {
        cb(null,no_rows_affected);
    }).catch( function (err) {
        cb(err,null);
    });
};


exports.findsub_category = function findsub_category(query, cb) {
    sub_categoryModel.findAll({
        where: query
    }).then(function (sub_category) {
        cb(null,sub_category);
    }).catch( function (err) {
        cb(err,null);
    });
};

exports.updatesub_category = function updatesub_category(id, updates, cb) {
    sub_categoryModel.update(updates,{where: {'id':id}}).then( function (rows_updated) {
        cb(null,rows_updated);
    }).catch(function (err) {
        cb(err,null);
    });
};