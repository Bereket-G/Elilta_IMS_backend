'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('projectitems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      project_Id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
              model: "projects",
              key: "ID"
          },
          onDelete: 'CASCADE'
      },
        itemId: {
          type: Sequelize.INTEGER,
        allowNull: false,
          references: {
              model: "items",
              key: "id"
          },
          onDelete: 'CASCADE'
      },
      quantity: {
        type: Sequelize.BIGINT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('projectitems');
  }
};