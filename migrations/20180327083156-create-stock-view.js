'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
    // logic for transforming into the new state
    return queryInterface.sequelize.query('CREATE VIEW ims_elilta.stock_view AS SELECT SUM(stock_items.quantity * item_prices.value) AS stock_calculated_value, SUM(stock_items.quantity) AS items_available, SUM(stock_items.value) AS value_of_stock FROM \n' +
        'stock_items INNER JOIN item_prices ON (item_prices.item_id = stock_items.itemId ) WHERE item_prices.name = \'Price1\';')
},

    down: (queryInterface, Sequelize) => {
    // logic for reverting the changes
    return queryInterface.sequelize.query('DROP VIEW stock_view;');
  }
}