'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('categories', [{
        name: 'Profile',
        createdAt : new Date(),
        updatedAt : new Date()
      }, {
        name: 'Accessory',
        createdAt : new Date(),
        updatedAt : new Date()
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    var x = "Profile";
    var y = "Accessory";
    return queryInterface.sequelize.query('DELETE FROM `categories` WHERE name = \'Profile\' OR name=\'Accessory\'');
  }
};
